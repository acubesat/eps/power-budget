#!/usr/bin/bash

COMMON_ARGS="--hide-plots --output-dir comparisons_orbits --eclipse scenarios/EPS_NanoAvionics.yml"

# Different orbits
xargs -I CMD --max-procs=2 bash -c CMD << EOC
./power_budget.py $COMMON_ARGS -a "attitude:sunpointing" \"ltan: \'06:00\'\" \"altitude:500km\" \"attitude:0deg\" --label \"06:00 LTAN\"
./power_budget.py $COMMON_ARGS -a "attitude:sunpointing" \"ltan: \'09:00\'\" \"altitude:500km\" \"attitude:0deg\" --label \"09:00 LTAN\"
./power_budget.py $COMMON_ARGS scenarios/Nadir_Pointing.yml -a \"ltan: \'10:00\'\" --label \"10:00 LTAN\"
./power_budget.py $COMMON_ARGS -a \"orbit: ISS\" --label \"ISS\"
./power_budget.py $COMMON_ARGS scenarios/Sun_Pointing.yml -a \"ltan: \'11:00\'\" --label \"11:00 LTAN\"
./power_budget.py $COMMON_ARGS scenarios/Sun_Pointing.yml -a \"ltan: \'12:00\'\" --label \"12:00 LTAN\"
EOC

COMMON_ARGS="scenarios/EPS_NanoAvionics.yml --hide-plots --output-dir comparisons"

# Different attitudes
# xargs -I CMD --max-procs=2 bash -c CMD << EOC
# ./power_budget.py $COMMON_ARGS -a "attitude:sunpointing" \"ltan: \'11:00\'\" "roll:APE" "yaw:APE" --label \"Sun Pointing \(roll + yaw error\)\"
# ./power_budget.py $COMMON_ARGS -a "attitude:sunpointing" "roll:APE" "yaw:~" --label \"Sun Pointing \(roll error\)\"
# ./power_budget.py $COMMON_ARGS -a "attitude:sunpointing" --label \"Sun Pointing \(precise\)\"
# ./power_budget.py $COMMON_ARGS -a "attitude:0deg" --label \"Nadir Pointing\"
# ./power_budget.py $COMMON_ARGS -a "attitude:tumbling" --label \"Spacecraft tumbling\"
# EOC

# Instantaneous experiments
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/init_only.yml --hide-plots --output-dir comparisons_instant -a "ltan: '11:00'" "attitude:sunpointing"
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/init_only.yml --hide-plots --output-dir comparisons_instant -a "ltan: '11:00'" "attitude:nadirpointing"
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/init_only.yml --hide-plots --output-dir comparisons_instant -a "ltan: '11:00'" "attitude:tumbling"

#./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Tumbling.yml --hide-plots -a "ltan: '10:00'" "duration: ~" "altitude: 450km" --label "Tumbling 10:00"

# Sun pointing dance
xargs -I CMD --max-procs=3 bash -c CMD << EOC
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Nadir_Pointing.yml --hide-plots -a \"ltan: \'10:00\'\" --label \"Nadir Pointing 10:00\"
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Sun_Pointing.yml --hide-plots -a \"ltan: \'11:00\'\" --label \"Sun Pointing 11:00\"
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Sun_Pointing.yml --hide-plots -a \"ltan: \'12:00\'\" --label \"Sun Pointing 12:00\"
EOC

# Sun pointing only (for excel sheet)
xargs -I CMD --max-procs=2 bash -c CMD << EOC
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Sun_Pointing_Only.yml --hide-plots -a \"ltan: \'11:00\'\" --label \"Sun Pointing Only 11:00\"
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Sun_Pointing_Only.yml --hide-plots -a \"ltan: \'12:00\'\" --label \"Sun Pointing Only 12:00\"
EOC

# Tumbling
xargs -I CMD --max-procs=3 bash -c CMD << EOC
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Tumbling.yml --hide-plots -a \"ltan: \'10:00\'\" --label \"Tumbling 10:00\"
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Tumbling.yml --hide-plots -a \"ltan: \'11:00\'\" --label \"Tumbling 11:00\"
./power_budget.py scenarios/EPS_NanoAvionics.yml scenarios/Tumbling.yml --hide-plots -a \"ltan: \'12:00\'\" --label \"Tumbling 12:00\"
EOC
