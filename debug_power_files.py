#!/usr/bin/python
import argparse
import logging
import datetime
import lib.files
import lib.scenarios
import sys
import jsbeautifier
import coloredlogs
import json
import os
from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import TerminalFormatter

parser = argparse.ArgumentParser(description='Power Generation Budget')
parser.add_argument('attribute', help='JSON for attributes to search', nargs='*')
args = parser.parse_args()

coloredlogs.install(level=logging.DEBUG, logger=logging.getLogger())

all_files = lib.files.get_all_files()
all_files = {os.path.basename(name): val for name, val in all_files.items()}

opts = jsbeautifier.default_options()
opts.indent_size = 2
print(highlight(jsbeautifier.beautify(json.dumps(all_files, ensure_ascii=False)), JsonLexer(), TerminalFormatter()))

if args.attribute:
    attributes = lib.scenarios.parse_attributes(args.attribute)
    analysis = lib.files.locate_analysis_by_attributes(attributes["common_attributes"])
    print(analysis)