import logging
import os
import re
import pandas
import pickle
import glob
import itertools
import lib.processor

ANALYSIS_DIRECTORY = os.path.dirname(os.path.realpath(__file__)) + "/../analyses"


def get_all_files():
    """Returns a dictionary with the attributes of all files in the analysis folder"""
    files = glob.glob(ANALYSIS_DIRECTORY + "/*.csv*")

    return {filename: get_attributes(filename) for filename in files}

def locate_analysis_by_attributes(search_attributes):
    """Search a matching .csv analysis filename in the analysis folder"""
    analyses = get_all_files()

    # Find which attributes do not exist in any of the files, and can be ignored
    existing_attributes = set(itertools.chain.from_iterable([analysis.keys() for analysis in analyses.values()]))
    for attribute in search_attributes.copy():
        if attribute not in existing_attributes:
            del search_attributes[attribute]

    # Function which returns true if the searched attributes are all found in a CSV file
    attributes_match = lambda search, file: all(map(lambda attr: (attr in file and search[attr] == file[attr]) or (attr not in file and search[attr] == None), search.keys()))

    # Return the first file that matches the search attributes
    matched_filenames = [filename for filename, file_attributes in analyses.items() if attributes_match(search_attributes, file_attributes)]
    try:
        if len(matched_filenames) > 1:
            logging.warning("Found too many .csv files with the requested arguments {}, picking first".format(search_attributes))
        return matched_filenames[0]
    except:
        for filename, attributes in analyses.items():
            logging.debug("Found analysis {} with {}".format(os.path.basename(filename), attributes))
        raise ValueError("Could not find .csv analysis with {}".format(search_attributes))


def get_human(attributes):
    """Get a human-readable level from a CSV attribute list"""
    return " ".join(attributes.values())

def get_attributes(data_path):
    """Get a dictionary of useful information (attitude, altitude, LTAN) from a CSV file path"""

    # Remove any extensions/directories
    filename = get_label(data_path)

    data = {}

    if re.search(r"(SSO|LTAN)", filename):
        data["orbit"] = "SSO"

        if ltan := re.search(r"LTAN\_?(\d{1,2})(?!\d)", filename):
            data["ltan"] = "{:02d}:00".format(int(ltan[1]))

        if ltan := re.search(r"(\d{1,2})[\:\_](\d{2})(?!\d)", filename):
            data["ltan"] = "{:02d}:{:02d}".format(int(ltan[1]), int(ltan[2]))

        if ltan := re.search(r"(\d{4})(?!\d)", filename):
            value = ltan[1]
            data["ltan"] = "{:02d}:{:02d}".format(int(value[0:2]), int(value[2:4]))
    elif re.search(r"ISS", filename):
        data["orbit"] = "ISS"

    if altitude := re.search(r"([0-9]{3})km", filename):
        data["altitude"] = altitude[0]

    if duration := re.search(r"([0-9]+)\s*year", filename):
        data["duration"] = "{:d} years".format(int(duration[1]))

    if roll := re.search(r"roll([^_]+)", filename):
        data["roll"] = roll[1]
    if pitch := re.search(r"pitch([^_]+)", filename):
        data["pitch"] = pitch[1]
    if yaw := re.search(r"yaw([^_]+)", filename):
        data["yaw"] = yaw[1]

    if degrees := re.search(r"(\d+)deg", filename):
        data["attitude"] = "{}°".format(degrees[1])
    elif attitude := re.search(r"[a-zA-Z]+ing", filename):
        data["attitude"] = attitude[0].lower()

    return data

def get_label(data_path):
    """Get a concise label from an input CSV file path"""
    return os.path.splitext(os.path.basename(data_path))[0].rstrip('.csv')


def parse_file(data_path):
    """Get a pandas dataset from an input CSV file path"""

    # Convert any given pickle paths to data paths, in order to make Tab autocompletion easier
    # in user shells
    data_path = data_path.rstrip(".pickle")

    candidates = [
        data_path,
        ANALYSIS_DIRECTORY + "/" + data_path
    ]

    found = False
    for candidate_path in candidates:
        pickle_path = candidate_path + ".pickle"

        pickle_exists = os.path.isfile(pickle_path)
        file_exists = os.path.isfile(candidate_path)

        if not pickle_exists and not file_exists:
            continue
        else:
            data_path = candidate_path
            found = True
            break

    if not found:
        raise RuntimeError("Could not find requested file {}".format(data_path))

    need_to_repickle = file_exists and (not pickle_exists or os.path.getmtime(data_path) > os.path.getmtime(pickle_path))

    pickle_file = open(pickle_path, "ab+")
    pickle_file.seek(0)

    if need_to_repickle:
        logging.warning('Pickle not up to date, creating from CSV...')

        df = pandas.read_csv(data_path, encoding="utf-16", dtype='unicode', header=0, na_filter=False, low_memory=False)
        df.columns = ['Pw_index', 'Panel', 'Index', 'Datetime', 'Power']
        df['Datetime'] = pandas.to_datetime(df['Datetime'], format='%m/%d/%Y %H:%M:%S.%f', errors='coerce')
        df['Panel'] = df['Panel'].apply(str)
        df['Power'] = df['Power'].astype(float)

        pickle_file.seek(0)
        pickle.dump(df, pickle_file)
    else:
        logging.debug('Reading pickle...')
        df = pickle.load(pickle_file)

    pickle_file.close()

    return (data_path, df)

