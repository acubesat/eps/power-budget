import logging
import os
import sys
import lib.files
import numpy as np
import pandas
import yaml
import re
import math
import jsonschema
import cexprtk
from tqdm import tqdm

SCHEMA_FILE = os.path.dirname(os.path.realpath(__file__)) + "/scenario.schema.yml"
MINUTES_IN_DAY = 24 * 60
VALUES_PER_MINUTE = 1
VALUES_PER_DAY = MINUTES_IN_DAY * VALUES_PER_MINUTE

def parse_scenario_simple(filenames):
    """Parse a scenario and include requested files, without analysing timelines and extrapolating data"""
    scenario_complete = {}

    if isinstance(filenames, str):
        filenames = [filenames]

    for filename in filenames:
        with open(filename, "r") as file:
            scenario = yaml.safe_load(file)
        with open(SCHEMA_FILE, "r") as schema:
            try:
                jsonschema.validate(scenario, yaml.safe_load(schema))
            except jsonschema.exceptions.ValidationError as e:
                logging.critical("There was an error while reading the {} scenario file in `{}`:".format(filename, ".".join(e.path)))
                logging.critical(e.message)
                logging.warning("Run the script -d to see the full error output")
                logging.warning("Make sure that you provide a well-formatted scenario file next time.")

                print(e.instance)

                # Throw back original exception, making the output a little bit more quiet
                # to prevent scaring users
                if logging.root.level > logging.DEBUG:
                    sys.tracebacklimit = -1
                    e.instance = filename
                    e.schema = SCHEMA_FILE
                raise e

        if "scenario" in scenario and "include" in scenario["scenario"]:
            includes = scenario["scenario"]["include"]
            includes.reverse()
            for include in includes:
                path = os.path.dirname(filename) + "/" + include # Paths are relative to current file path
                scenario = merge_dictionaries(parse_scenario_simple(path), scenario, [ 'scenario', 'timeline' ])

        scenario_complete = merge_dictionaries(scenario_complete, scenario, [ 'scenario', 'timeline' ])

    return scenario_complete


def parse_scenario(filenames, overrides={}):
    """Parses scenario YAML files into a dictionary"""

    scenario = parse_scenario_simple(filenames)
    scenario = merge_dictionaries(scenario, overrides)
    scenario_raw = scenario.copy()

    range_pattern = re.compile(r"(.+)\s*[-~]\s*(.+)")

    # Parse the timeline into a format that can be read by python
    if "timeline" in scenario:
        transitions = []

        for phase, properties in scenario["timeline"].items():
            # Convert input range to pandas dataframe indexes
            if timeframe := range_pattern.match(phase): # Numbers can be a lot of stuff, so we're matching with .+
                days = (float(timeframe[1]), float(timeframe[2]))
                indexes = (math.floor(days[0] * VALUES_PER_DAY), math.floor(days[1] * VALUES_PER_DAY))
            else:
                raise ValueError("Cannot parse timeline phase {}".format(phase))

            if "periodic" in properties:
                for period, period_properties in properties["periodic"].items():
                    if timeframe := range_pattern.match(period): # Numbers can be a lot of stuff, so we're matching with .+
                        period_properties["minutes"] = (float(timeframe[1]), float(timeframe[2]))
                    else:
                        raise ValueError("Cannot parse timeline period {}".format(period))

                total_minutes = (days[1] - days[0]) * MINUTES_IN_DAY
                t = 0
                periods = list(properties["periodic"].items())
                period_count = len(periods)
                current_period = 0
                while t < total_minutes:
                    current_period_properties = periods[current_period][1]
                    period_duration_minutes = current_period_properties["minutes"][1] - current_period_properties["minutes"][0]

                    minutes = np.array([0, period_duration_minutes])
                    minutes += t # Add start of current period inside phase
                    minutes += days[0] * MINUTES_IN_DAY # Add start of current phase
                    indexes = tuple(np.floor(minutes * VALUES_PER_MINUTE).astype(int))

                    # Collect all the attributes relevant to the orbit, and not other time-dependent aspects
                    current_attributes = {**properties, **current_period_properties}
                    search_attributes = {**scenario["common_attributes"], **current_attributes}
                    current_attributes["analysis_file"] = lib.files.locate_analysis_by_attributes(search_attributes)
                    current_attributes["indexes"] = indexes
                    transitions.append((indexes[0], current_attributes))

                    t += period_duration_minutes
                    current_period = (current_period + 1) % period_count
            else:
                # Collect all the attributes relevant to the orbit, and not other time-dependent aspects
                search_attributes = {**scenario["common_attributes"], **properties}
                properties["analysis_file"] = lib.files.locate_analysis_by_attributes(search_attributes)
                properties["indexes"] = indexes
                transitions.append((indexes[0], properties))

        scenario["transitions"] = transitions
        scenario["total_values"] = days[1] * VALUES_PER_DAY

    return (scenario, scenario_raw)

def generate_power_consumption(scenario):
    """Return a numpy series consisting of the power consumption values of the scenario at each specific point in orbit"""
    transitions = scenario["transitions"]
    value_count = transitions[-1][1]["indexes"][1]
    power_consumption = np.full((value_count,), np.nan)

    for _, transition in transitions:
        power_consumption[transition["indexes"][0]:transition["indexes"][1]] = scenario["modes"][transition["mode"]]["consumption"]

    return power_consumption

def parse_multiple_files(transitions):
    """Return a pandas dataset consisting of inputs from multiple .csv files which are used for different timepoints

    Arguments:
    values -- An ordered list of (file, start_date) tuples
    """

    all_dataframes = {}
    min_index = None
    max_index = None

    # Pre-parsing of all transitions
    for _, attributes in transitions:
        data_file = attributes["analysis_file"]

        # Add new dataframe to all_dataframes
        if not data_file in all_dataframes:
            all_dataframes[data_file] = lib.processor.Processor(lib.files.parse_file(data_file)[1])
            all_dataframes[data_file].source_indexes = []
            all_dataframes[data_file].target_indexes = []
            logging.info('Using file {}'.format(lib.files.get_label(data_file)))

        if min_index is None or attributes["indexes"][0] < min_index:
            min_index = attributes["indexes"][0]
        if max_index is None or attributes["indexes"][1] > max_index:
            max_index = attributes["indexes"][1]
    first_dataframe = next(iter(all_dataframes.values()))

    df = pandas.DataFrame(columns=first_dataframe.df.columns,index=first_dataframe.df.index,dtype=float)
    df['Panel'] = first_dataframe.df['Panel']
    df['Datetime'] = first_dataframe.df['Datetime']

    temp_container = np.full((len(first_dataframe.df.index),), np.nan)

    target_indexes = first_dataframe.get_indexes()

    logging.info("Preparing scenario timeline")
    for _, attributes in transitions:
        transition_offset = attributes["indexes"]
        current_df = all_dataframes[attributes["analysis_file"]]

        source_shifted_indexes = {
            key: range(value[0] + transition_offset[0], value[0] + transition_offset[1])
            for key, value in current_df.get_indexes().items()
        }

        target_shifted_indexes = {
            key: range(value[0] + transition_offset[0], value[0] + transition_offset[1])
            for key, value in target_indexes.items()
        }

        for key in source_shifted_indexes.keys():
            current_df.source_indexes += source_shifted_indexes[key]
            current_df.target_indexes += target_shifted_indexes[key]

    for current_df in all_dataframes.values():
        temp_container[current_df.target_indexes] = current_df.df.loc[current_df.source_indexes,'Power']

    df.loc[:,'Power'] = temp_container

    # Remove NaN values
    df = df[df['Power'].notna()]

    return df

def merge_dictionaries(a, b, no_merge=[], path=None):
    """Performs a deep merge of dictionaries a and b. no_merge controls key names that should not be merged. b overwrites a"""
    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict) and key not in no_merge:
                merge_dictionaries(a[key], b[key], [], path + [str(key)])
            elif a[key] == b[key]:
                pass # same leaf value
            else:
                # Values of B take precedence over values of A
                a[key] = b[key]
        else:
            a[key] = b[key]
    return a

def parse_attributes(attr_argument):
    """Parses a list of user-provided attributes, and generates a relevant dictionary"""

    if attr_argument is None:
        return {}

    attributes = {}

    for input_text in attr_argument:
        input_text = input_text.replace("deg", "°") # Make the users not have to type ° every time :)
        attribute = yaml.safe_load(input_text)
        if not isinstance(attribute, dict):
            # Try adding a space after colons to make this a valid object
            input_text = input_text.replace(":", ": ")
            attribute = yaml.safe_load(input_text)
        if not isinstance(attribute, dict):
            raise ValueError("You have given me a wrong argument. `{}` is not an object.".format(attribute))
        attributes = merge_dictionaries(attributes, attribute)

    if attributes != {}:
        logging.info("User-provided attributes: {}".format(attributes))

    return {
        "common_attributes": attributes
    }

class ExprTag(yaml.YAMLObject):
    yaml_tag = u'!Expr'

    def __init__(self, expression):
        self.expression = expression
        text = expression[0] if isinstance(expression, list) else expression
        self.result = cexprtk.evaluate_expression(text.lstrip('='), {})

    def __repr__(self):
        return str(self.result)

    @classmethod
    def from_yaml(cls, loader, node):
        return ExprTag(node.value).result

    @classmethod
    def to_yaml(cls, dumper, data):
        return dumper.represent_scalar(cls.yaml_tag, data.expression)

yaml.SafeLoader.add_constructor(ExprTag.yaml_tag, ExprTag.from_yaml)
yaml.SafeDumper.add_representer(ExprTag, ExprTag.to_yaml)
# Every string that starts with `=` gets evaluated as an expression
yaml.add_implicit_resolver(ExprTag.yaml_tag, re.compile(r'^=.*$'), Loader=yaml.SafeLoader)