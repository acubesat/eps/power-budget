import pandas


class Processor:
    def __init__(self, dataframe):
        self.df = dataframe

        # Process indexes for each analysis group
        start_indexes = self.df["Panel"].drop_duplicates(keep='first')
        stop_indexes = self.df["Panel"].drop_duplicates(keep='last')
        group_names = start_indexes.to_numpy()
        indexes = {}

        for id, _ in enumerate(start_indexes.index):
            indexes[group_names[id]] = (
                (start_indexes.index[id], stop_indexes.index[id]))

        self.indexes = indexes

    def get_total_indexes(self):
        """Returns a tuple with the first and last indexes of the "All Solar Panel Groups" total element"""
        return self.indexes["All Solar Panel Groups"]

    def get_indexes(self, assignment=None):
        """Returns the indexes for each solar panel. If assignment is provided, performs a rename"""

        indexes = self.indexes

        if assignment is not None:
            inverted_assignment = {v: k for k, v in assignment.items()}
            indexes = dict((inverted_assignment[key], val)
                           for key, val in indexes.items())

        return indexes




