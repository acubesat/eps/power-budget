import csv
import numpy as np
from scipy import interpolate
import logging
import os

EFFICIENCIES_DIRECTORY = os.path.dirname(os.path.realpath(__file__)) + "/../efficiencies/"

class MPPTCalculator:
    def __init__(self, source, fast=False):
        self.source = source
        self.fast = fast
        self.x_data = {}
        self.y_data = {}

        self.interpolation = {}

        # Load all data initially to prevent breaking
        if source == "ISIS":
            volts = {
                '5V': 5,
                '7.5V': 7.5,
                '10V': 10,
                '12.5V': 12.5,
                '14V': 14
            }

            for filename, value in volts.items():
                with open(EFFICIENCIES_DIRECTORY + 'isis_mppt_' + filename + '.txt') as csv_file:
                    data = csv.reader(csv_file, delimiter=',')
                    col_x = []
                    col_y = []
                    for row in data:
                        col_x.append(float(row[0]))
                        col_y.append(float(row[1]))
                    self.x_data[value] = col_x
                    self.y_data[value] = col_y

        elif source == "NanoAvionics":
            amps = {
                '0.3A': 0.3,
                '1A': 1,
                '2.5A': 2.5,
                '4A': 4,
            }

            for filename, value in amps.items():
                with open(EFFICIENCIES_DIRECTORY + 'nanoavionics_mppt_' + filename + '.txt') as csv_file:
                    data = csv.reader(csv_file, delimiter=',')
                    col_x = []
                    col_y = []
                    for row in data:
                        col_x.append(float(row[0]))
                        col_y.append(float(row[1]))
                    self.x_data[value] = col_x
                    self.y_data[value] = col_y

        logging.debug("Read MPPT values for {}".format(source))

        # Extrapolate the values
        self.extrapolate_values()
        self.generate_interpolations()

    # Extrapolates values
    def extrapolate_values(self):
        max_total = 0

        # Get the max value for all keys
        for key in self.x_data.keys():
            max_value = self.x_data[key][-1]

            if max_value > max_total:
                max_total = max_value

        # Add the max value to keys that don't have it
        for key in self.x_data.keys():
            max_value = self.x_data[key][-1]
            min_value = self.x_data[key][0]

            if max_value < max_total:
                # Extrapolation doesn't work very much for faraway variables
                extrapolated = interpolate.interp1d(self.x_data[key], self.y_data[key], fill_value = "extrapolate")(max_total)
                self.x_data[key].append(max_total)
                self.y_data[key].append(extrapolated)

            if min_value > 0:
                extrapolated = interpolate.interp1d(self.x_data[key], self.y_data[key], fill_value = "extrapolate")(0)
                extrapolated = self.y_data[key][0]
                self.x_data[key].insert(0, 0)
                self.y_data[key].insert(0, extrapolated)

    def generate_interpolations(self):
        for key in self.x_data.keys():
            self.interpolation[key] = interpolate.interp1d(self.x_data[key], self.y_data[key])

    def get_efficiency_graph(self, value):
        all_values = list(self.x_data.keys())
        for i in range(len(all_values) - 1):
            if all_values[i + 1] >= value or i == len(all_values) - 2:
                return (
                    (all_values[i], self.x_data[all_values[i]], self.y_data[all_values[i]]),
                    (all_values[i + 1], self.x_data[all_values[i + 1]], self.y_data[all_values[i + 1]])
                )
        raise ValueError('Cannot find graph for value {}'.format(value))

    def find_indexes(self, graph, value):
        if self.fast:
            start = graph[1]
            step = (graph[-2] - graph[1]) / (len(graph) - 3)
            index = int(np.ceil((value - start) / step + 1))
        else:
            index = np.searchsorted(graph, value, side='left')

        if index <= 0:
            index = 1

        if index >= len(graph):
            index = len(graph) - 1

        return (index - 1, index)

    # Does binary search to find the correct index
    # TODO: Rename to something more readable and faster
    def ranger_function(self, rng, constant):
        for i in range(0, len(rng)-1):
            if (rng[i] <= constant) and (rng[i+1] >= constant):
                index = i
                return index

    def simple_linear_interpolation(self, x, x0, y0):
        if x > x0[1]:
            # Right clip check
            return y0[1]

        return y0[0] + (x - x0[0]) * (y0[1] - y0[0]) / (x0[1] - x0[0])

    def simple_nearest_neighbour(self, x, x0, y0):
        return y0[0]

    def scipy_linear_interpolation(self, x, key):
        return self.interpolation[key](x)

    def efficiency(self, volts, amperes):
        if amperes < 1e-4:
            return 1

        if self.source == "ISIS":
            graphs = self.get_efficiency_graph(volts)
            indexes_first = self.find_indexes(graphs[0][1], amperes)
            indexes_last = self.find_indexes(graphs[1][1], amperes)

            # interpolated_first = self.scipy_linear_interpolation(amperes, graphs[0][0])
            # interpolated_last = self.scipy_linear_interpolation(amperes, graphs[1][0])

            interpolated_first = self.simple_linear_interpolation(amperes, [graphs[0][1][indexes_first[0]], graphs[0][1][indexes_first[1]]], [graphs[0][2][indexes_first[0]], graphs[0][2][indexes_first[1]]])
            interpolated_last = self.simple_linear_interpolation(amperes, [graphs[1][1][indexes_last[0]], graphs[1][1][indexes_last[1]]], [graphs[1][2][indexes_last[0]], graphs[1][2][indexes_last[1]]])

            return self.simple_linear_interpolation(volts, [graphs[0][0], graphs[1][0]], [interpolated_first, interpolated_last])
        elif self.source == "NanoAvionics":
            graphs = self.get_efficiency_graph(amperes)
            indexes_first = self.find_indexes(graphs[0][1], volts)
            indexes_last = self.find_indexes(graphs[1][1], volts)

            # interpolated_first = self.scipy_linear_interpolation(volts, graphs[0][0])
            # interpolated_last = self.scipy_linear_interpolation(volts, graphs[1][0])

            interpolated_first = self.simple_linear_interpolation(volts, [graphs[0][1][indexes_first[0]], graphs[0][1][indexes_first[1]]], [graphs[0][2][indexes_first[0]], graphs[0][2][indexes_first[1]]])
            interpolated_last = self.simple_linear_interpolation(volts, [graphs[1][1][indexes_last[0]], graphs[1][1][indexes_last[1]]], [graphs[1][2][indexes_last[0]], graphs[1][2][indexes_last[1]]])

            return self.simple_linear_interpolation(amperes, [graphs[0][0], graphs[1][0]], [interpolated_first, interpolated_last])

if __name__ == "__main__":
    import matplotlib.pyplot as plt

    cell_voltage = 2.14
    used_voltages = [cell_voltage * 2, cell_voltage * 3, cell_voltage * 4, cell_voltage * 6, cell_voltage * 7]

    # x = np.linspace(0.1, 1.6, 100)
    # calculator = MPPTCalculator("ISIS")
    # mppt_calculator = np.vectorize(lambda x, v: calculator.efficiency(v, x))

    # for i in np.linspace(5,14,30):
    #     plt.plot(x, mppt_calculator(x, i), label=i)

    x = np.linspace(0.1, 2, 100)
    calculator = MPPTCalculator("NanoAvionics", False)
    mppt_calculator = np.vectorize(lambda x, v: calculator.efficiency(x, v))

    for i in used_voltages:
        plt.plot(x, mppt_calculator(i, x), label='NanoAvionics {} V ({:d} cells)'.format(i, round(i / cell_voltage)))

    x = np.linspace(0.1, 2, 100)
    calculator = MPPTCalculator("ISIS", False)
    mppt_calculator = np.vectorize(lambda x, v: calculator.efficiency(x, v))

    for i in used_voltages:
        plt.plot(x, mppt_calculator(i, x),
            '--', linewidth=2,
            label='ISIS {} V ({:d} cells)'.format(i, int(i // cell_voltage))
        )

    plt.xlabel('I (A)')
    plt.ylabel('Efficiency')
    plt.title('MPPT efficiency')
    plt.legend()
    plt.grid()
    plt.show()


