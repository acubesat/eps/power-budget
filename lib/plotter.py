import csv
import numpy as np
from scipy import interpolate
import logging
import os
import matplotlib.pyplot as plt

class Plotter:
    def __init__(self, title, output_filename, hide=False):
        self.title = title
        self.output_filename = output_filename
        self.hide = hide

    def new(self, id=None):
        """Create a new figure with a specified ID by calling plt.figure()"""
        return plt.figure(id, figsize=(19.2, 10.8))

    def plot(self, plot, short_name="", long_name=""):
        """Save or display the plot on the screen"""
        filename = self.output_filename + ".{}".format(short_name).rstrip(".") + ".pdf"

        plot.suptitle("{} — {}".format(self.title, long_name), fontsize=16, y=1.05)
        plot.savefig(filename)

        if not self.hide:
            plot.show()
            plt.draw()
            plt.pause(.001)
