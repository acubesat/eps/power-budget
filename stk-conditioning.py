#!/usr/bin/env python
import argparse
import os
import io
import logging
import coloredlogs

coloredlogs.install(level=logging.DEBUG)

parser = argparse.ArgumentParser(description='Script to convert .dat files from STK power generation analysis to .csv, in order to feed into the power budget')
parser.add_argument('dat_file', nargs='+', help='The raw .dat file extracted from STK')

args = parser.parse_args()

for dat_file in args.dat_file:
    # renamee is the file getting renamed, pre is the part of file name before extension and ext is current extension
    pre, ext = os.path.splitext(dat_file)
    csv_file = pre + ".csv"

    logging.info("Processing file {} -> {}".format(dat_file, csv_file))

    with open(dat_file, "r", encoding='utf-16') as f:
        lines = f.readlines()

    logging.debug("Processing lines")

    # Find if the file contains the header line
    if lines[0].strip() != "Pw_index,Panel,Index,Datetime,Powers":
        lines.insert(0, "Pw_index,Panel,Index,Datetime,Powers\r\n")

    for index, line in enumerate(lines):
        lines[index] = line.replace(' - ', ',')

    logging.debug("Writing new file")

    with open(csv_file, "w", encoding='utf-16', newline="\r\n") as f:
        for line in lines:
            if not ', ,' in line:
                f.write(line)

    # with open("yourfile.txt", "r") as f:
    #     lines = f.readlines()
    # with open("yourfile.txt", "w") as f:
    #     for line in lines:
    #         if line.strip("\n") != "nickname_to_delete":
    #             f.write(line)
