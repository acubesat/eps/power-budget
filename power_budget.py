#!/usr/bin/python
import argparse
import logging
import datetime
import pickle
import coloredlogs
import jsbeautifier
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import numpy as np
import pandas
import os
import lib.files
import lib.plotter
import lib.processor
import lib.scenarios
from scipy import stats
from tqdm import trange, tqdm
from matplotlib.axes._axes import _make_inset_locator
from lib.mppt_calculator import MPPTCalculator
from collections import namedtuple
from munch import munchify
import json
import sys

# Command-line options
parser = argparse.ArgumentParser(description='Power Generation Budget')
parser.add_argument('scenario', help='The .yml scenario file. If multiple files are provided, they are loaded consecutively.', nargs='+')
parser.add_argument('--eclipse', help='Generate eclipse/sunlight time statistics', action='store_true')
parser.add_argument('--hide-plots', help='Do not display and pause for plots', action='store_true')
parser.add_argument('--label', help='A custom label for the data, to be used in plots and file names')
parser.add_argument('--output', '-o', help='The title of the files to store the plot outputs and PDFs. Is generated automatically if not provided')
parser.add_argument('--output-dir', help='The output directory of generated files and PDFs', default='outputs')
parser.add_argument('--fast-mppt', help='Perform less accurate but faster MPPT calculations', action='store_true')
parser.add_argument('--demo', help='Perform instant demo calculations for testing purposes', action='store_true')
parser.add_argument('--debug', '-d', help='Enable debug logging', action='store_true')
parser.add_argument('--attr', '-a', help='Override an attribute from the scenario. Provide valid YAML/JSON as input', nargs='+')
args = parser.parse_args()

coloredlogs.install(level=logging.DEBUG if args.debug else logging.INFO, logger=logging.getLogger())
logging.getLogger('matplotlib').setLevel(logging.WARNING) # Remove annoying log messages from matplotlib

datapack = {}
datapack["options"] = vars(args)
datapack["date"] = datetime.datetime.now().astimezone().replace(microsecond=0).isoformat()

overrides = lib.scenarios.parse_attributes(args.attr)
scenario, scenario_raw = lib.scenarios.parse_scenario(args.scenario, overrides)

variables = munchify(scenario["common_attributes"])
variables.orbit_duration = int(variables.orbit_duration)
if not hasattr(variables, 'orbit_averaging'):
    variables.orbit_averaging = None
datapack["variables"] = {key: getattr(variables, key) for key in dir(variables) if not key.startswith('_')}

datapack["scenario"] = scenario_raw

if args.label:
    data_label = args.label
    data_label_human = args.label
else:
    try:
        data_label = scenario["scenario"]["name"] if "name" in scenario["scenario"] else " ".join(overrides["common_attributes"].values())
        data_label_human = data_label
    except:
        t, v, tb = sys.exc_info()
        logging.warning("Could not infer useful data about the run: {} {}".format(t, v))
        logging.warning("I will continue running the script for you, but will not store cool fun facts.")
        logging.warning("Maybe you didn't run the correct scenario file?")
        data_label = os.path.basename(args.scenario[-1])
        data_label_human = data_label

logging.info('Power Budget Calculation for: {}'.format(data_label))

if args.output is None:
    args.output = args.output_dir.rstrip('/') + "/" + data_label
    if args.label is None:
        args.output += "-" + variables.eps_type
        if args.demo:
            args.output += "-" + "demo"
        elif args.fast_mppt:
            args.output += "-" + "fast"

mppt_calculator = MPPTCalculator(variables.eps_type, args.fast_mppt)

logging.info('Saving results as: {}'.format(args.output))

plotter = lib.plotter.Plotter(data_label_human, args.output, args.hide_plots)

# Power/Battery values

power_consumption_minutes = lib.scenarios.generate_power_consumption(scenario)

# Calculate MPPT

SOLAR_PANELS = {
    "All": "All Solar Panel Groups",
    "Y-": "AcubeSAT_YmFace_Cells",
    "Y+": "AcubeSAT_YpFace_Cells",
    "X-": "AcubeSAT_XmFace_Cells",
    "X+": "AcubeSAT_XpFace_Cells",
    "Z-": "AcubeSAT_ZmFace_Cells",
}

datapack["panels"] = {}
for panel in SOLAR_PANELS.keys():
    datapack["panels"][panel] = {}

# Number of Cells of each side in each configuration
MPPT_CONFIGURATION = pandas.DataFrame.from_dict(variables.mppt_configuration, orient='index')
# Convert arrays to tuples
MPPT_CONFIGURATION = MPPT_CONFIGURATION.applymap(lambda mppt: tuple(mppt) if type(mppt) is list else mppt)
# Convert all items to tuples
MPPT_CONFIGURATION_TUPLED = MPPT_CONFIGURATION.applymap(lambda mppt: () if mppt == 0 else mppt if type(mppt) is tuple else (mppt,))

mppt_strings = MPPT_CONFIGURATION_TUPLED.applymap(len).sum(axis=0)
mppt_cells = (MPPT_CONFIGURATION_TUPLED.applymap(sum).sum(axis=0) / mppt_strings).replace(np.NaN, 0).astype(int)
mppt_voltage = variables.solar_cells.voltage * mppt_cells
panel_cells = MPPT_CONFIGURATION_TUPLED.applymap(sum).sum(axis=1)
panel_voltage = variables.solar_cells.voltage * panel_cells

mppt_details = pandas.DataFrame({
    "strings": mppt_strings,
    "cells": mppt_cells,
    "voltage": mppt_voltage
})
mppt_count = MPPT_CONFIGURATION_TUPLED.count(axis=1)[0]

panel_details = pandas.DataFrame({
    "cells": panel_cells,
    "voltage": panel_voltage
})

thermal_factor = variables.solar_cells.voltage / variables.solar_cells.best_voltage * variables.solar_cells.current / variables.solar_cells.best_current

# Parsing of the data frame
df = lib.scenarios.parse_multiple_files(scenario["transitions"])
df_raw = df.copy()
processor = lib.processor.Processor(df)

all_start, all_end = processor.get_total_indexes()
panel_indexes = processor.get_indexes(SOLAR_PANELS)
index_count = panel_indexes["All"][1] - panel_indexes["All"][0] + 1

tqdm.pandas()


mppt_current = pandas.DataFrame(0, index=np.arange(index_count), columns=np.arange(mppt_count))
mppt_efficiency = pandas.DataFrame(0, index=np.arange(index_count), columns=np.arange(mppt_count))
mppt_power = pandas.DataFrame(0, index=np.arange(index_count), columns=np.arange(mppt_count))

for panel_alias, panel_fullname in SOLAR_PANELS.items():
    if panel_alias == "X-":
        indexes = panel_indexes[panel_alias]
        df.loc[indexes[0]:indexes[1], 'Power'] *= 7/6
    # print("WARNING!!!!!!!!!!!!!!!!!!!!!!!!!!!! INACCURATE BUDGETS!!!!!!!!!!!!!!!!!!1")
    # if panel_alias == "Y-":
    #     indexes = panel_indexes[panel_alias]
    #     df.loc[indexes[0]:indexes[1], 'Power'] *= 6/7
    # if panel_alias == "Y+":
    #     indexes = panel_indexes[panel_alias]
    #     df.loc[indexes[0]:indexes[1], 'Power'] *= 6/7
    if panel_alias != "All":
        indexes = panel_indexes[panel_alias]
        panel_power = df.loc[indexes[0]:indexes[1], 'Power']
        cell_current = panel_power / panel_details.voltage[panel_alias]

        cell_current = cell_current.clip(None, variables.solar_cells.current) * thermal_factor

        for mppt in MPPT_CONFIGURATION_TUPLED.columns:
            if MPPT_CONFIGURATION_TUPLED.loc[panel_alias,mppt] != ():
                mppt_current.loc[:,mppt] += len(MPPT_CONFIGURATION_TUPLED.loc[panel_alias,mppt]) * cell_current.to_numpy()

for mppt, details in mppt_details.iterrows():
    tqdm.pandas(desc="MPPT {}/{}".format(mppt, len(mppt_details)))

    if args.demo:
        mppt_efficiency.loc[:,mppt] = mppt_calculator.efficiency(details.voltage, mppt_current.loc[:,mppt].mean())
    else:
        mppt_efficiency.loc[:,mppt] = mppt_current.loc[:,mppt].progress_apply(lambda current: mppt_calculator.efficiency(details.voltage, current))
    mppt_power.loc[:,mppt] = mppt_efficiency.loc[:,mppt] * (details.voltage - variables.diode_drop) * mppt_current.loc[:,mppt]

df.loc[all_start:all_end, 'Power'] = mppt_power.sum(axis=1).to_numpy()

mppt_efficiency_orbit = mppt_efficiency.replace(1, np.NaN).groupby(np.arange(index_count) // variables.orbit_duration).mean()
plt_mppt = plotter.new(0)
ax_mppt = plt_mppt.subplots()

max_date = mppt_efficiency_orbit.axes[0].size * variables.orbit_duration
panel_dates_rolling = df["Datetime"].iloc[range(0, max_date, variables.orbit_duration)].to_numpy()

for mppt in mppt_efficiency_orbit.columns:
    ax_mppt.plot(panel_dates_rolling, mppt_efficiency_orbit.loc[:,mppt].to_numpy(), label='MPPT {}'.format(mppt))

ax_mppt.set_xlabel('Date', fontsize=22.5, y=0.01)
ax_mppt.set_ylabel('MPPT efficiency', fontsize=22.5, x=-0.01)
ax_mppt.grid()
ax_mppt.legend(fontsize=20, bbox_to_anchor=(0.97, 0.02), loc="lower right")
ax_mppt.tick_params(labelsize=14)

plt_mppt.tight_layout()
plotter.plot(plt_mppt, "mppt", "MPPT Efficiencies")


# Calculate the conditional mean value, for every panel and for all panels

SOLAR_PANELS = {
    "All": "All Solar Panel Groups",
    "Y-": "AcubeSAT_YmFace_Cells",
    "Y+": "AcubeSAT_YpFace_Cells",
    "X-": "AcubeSAT_XmFace_Cells",
    "X+": "AcubeSAT_XpFace_Cells",
    "Z-": "AcubeSAT_ZmFace_Cells",
}
PANEL_COUNT = len(SOLAR_PANELS)
INITIAL_DURATION = 500

panel_initial_dates = df["Datetime"].head(INITIAL_DURATION).to_numpy()

plt_initial = plotter.new(1)
plt_total = plotter.new(2)

ax_initial = plt_initial.subplots()
ax_total = plt_total.subplots()

for panel_alias, panel_fullname in SOLAR_PANELS.items():
    panel_values = df["Power"][df["Panel"] == panel_fullname]

    mean = panel_values.mean()
    maximum = panel_values.max()
    mean_sunlight = panel_values[panel_values > 1e-3].mean()

    print("Average/Sunlight/Max power generation through the whole orbit in {:>3}: {:.4f} W, {:.4f} W, {:.4f} W".format(
        panel_alias, mean, mean_sunlight, maximum
    ))

    datapack["panels"][panel_alias] = {
        "mean": mean,
        "mean_sunlight": mean_sunlight,
        "maximum": maximum,
    }

    if panel_alias == "All":
        # Calculates average for every ORBIT_DURATION elements. Last item is removed as it often uses 0 to replace
        # missing final values
        panel_values_rolling = panel_values.groupby(np.arange(len(panel_values)) // variables.orbit_duration).mean()[:-1]

        if variables.orbit_averaging is not None:
            panel_values_rolling_average = panel_values_rolling.groupby(np.arange(len(panel_values_rolling)) // variables.orbit_averaging).mean()[:-1]

            min_orbit_average_mean_power = panel_values_rolling_average.min()
            max_orbit_average_mean_power = panel_values_rolling_average.max()

            print("Min/Max OAMP in {:>3}: {:.4f} W, {:.4f} W".format(panel_alias, min_orbit_average_mean_power,
                                                                 max_orbit_average_mean_power))

            datapack["panels"][panel_alias]["min_oamp"] = min_orbit_average_mean_power
            datapack["panels"][panel_alias]["max_oamp"] = max_orbit_average_mean_power

        panel_values_rolling_without_outliers = panel_values_rolling[(np.abs(stats.zscore(panel_values_rolling)) < 2.2)]
        min_orbit_average_power = panel_values_rolling_without_outliers.min()
        max_orbit_average_power = panel_values_rolling_without_outliers.max()

        print("Min/Max OAP in {:>3}: {:.4f} W, {:.4f} W".format(panel_alias, min_orbit_average_power,
                                                                max_orbit_average_power))

        datapack["panels"][panel_alias]["min_oap"] = min_orbit_average_power
        datapack["panels"][panel_alias]["max_oap"] = max_orbit_average_power

        panel_values_raw = df_raw["Power"][df_raw["Panel"] == panel_fullname]
        panel_values_rolling_raw = panel_values_raw.groupby(np.arange(len(panel_values_raw)) // variables.orbit_duration).mean()[:-1]
        panel_values_rolling_without_outliers_raw = panel_values_rolling_raw[(np.abs(stats.zscore(panel_values_rolling_raw)) < 2.2)]
        min_orbit_average_power_raw = panel_values_rolling_without_outliers_raw.min()
        max_orbit_average_power_raw = panel_values_rolling_without_outliers_raw.max()
        print("Min/Max OAP without MPPT: {:.4f} W, {:.4f} W".format(min_orbit_average_power_raw, max_orbit_average_power_raw))

        max_date = len(panel_values_rolling) * variables.orbit_duration
        panel_dates_rolling = df["Datetime"].iloc[range(0, max_date, variables.orbit_duration)].to_numpy()

        if variables.orbit_averaging is not None:
            max_date_average = len(panel_values_rolling_average) * variables.orbit_averaging
            panel_dates_rolling_average = panel_dates_rolling[range(0, max_date_average, variables.orbit_averaging)]
            ax_total.plot(panel_dates_rolling, panel_values_rolling.to_numpy(), 'black', alpha=0.2, zorder=-10,
                        label='Orbit Average Power actual (incl. efficiency)')
            ax_total.plot(panel_dates_rolling_average, panel_values_rolling_average.to_numpy(), zorder=10,
                        label='Orbit Average Power mean (incl. efficiency)')
        else:
            ax_total.plot(panel_dates_rolling, panel_values_rolling.to_numpy(), zorder=10,
                        label='Orbit Average Power (incl. efficiency)')
        with open(args.output + ".plot.pickle", "wb") as f:
            if variables.orbit_averaging is not None:
                pickle.dump((panel_dates_rolling_average, panel_values_rolling_average.to_numpy()), f)
            else:
                pickle.dump((panel_dates_rolling, panel_values_rolling.to_numpy()), f)
        with open(args.output + ".consumption.pickle", "wb") as f:
            pickle.dump((df.Datetime.iloc[0:len(power_consumption_minutes)].to_numpy(), power_consumption_minutes), f)

        ax_total.axhline(y=min_orbit_average_power, color='gray', linestyle='--')
        ax_total.axhline(y=max_orbit_average_power, color='gray', linestyle='--')
        ax_total.plot(df.Datetime.iloc[0:len(power_consumption_minutes)].to_numpy(), power_consumption_minutes,
                      color='orange', label='Power Consumption\n(incl. efficiency + margin)')

        ax_initial.plot(panel_initial_dates, power_consumption_minutes[:INITIAL_DURATION], color='orange', zorder=-1,
                        linestyle='--', label='Power Consumption\n(incl. efficiency + margin)')
        ax_initial.plot(panel_initial_dates, panel_values.head(INITIAL_DURATION).to_numpy(), color='gray',
                        linewidth=3.0, label='All Panels')
    else:
        ax_initial.plot(panel_initial_dates, panel_values.head(INITIAL_DURATION).to_numpy(), label=panel_alias)

ax_total.set_xlabel('Date', fontsize=22.5, y=0.01)
ax_total.set_ylabel('Orbit Average Power [W]', fontsize=22.5, x=-0.01)
ax_total.grid()
ax_total.legend(fontsize=20, bbox_to_anchor=(0.97, 0.02), loc="lower right")
ax_total.tick_params(labelsize=14)

plt_total.tight_layout()
plotter.plot(plt_total, "total", "Mission lifetime")

ax_initial.set_xlabel('Date', fontsize=22.5, y=0.01)
ax_initial.set_ylabel('Instantaneous Panel Power, applied losses [W]', fontsize=22.5, x=-0.01)
ax_initial.grid()
ax_initial.tick_params(labelsize=14)
ax_initial.set_ylim(top=9)

ax_initial.legend(fontsize=20, bbox_to_anchor=(0.5, 1.1), loc="upper center", ncol=7, fancybox=True, framealpha=1)
plt_initial.tight_layout()
plotter.plot(plt_initial, "initial", "Initial orbits")

# Maximum and minimum eclipse and sunlight duration, average eclipse and sunlight duration

counter = 1
counter_eclipse = 0
counter_sun = 0
eclipse_max = 0
sun_max = 0
sun_min = 200
number_eclipses = 1
total_eclipse_time = 0
number_suns = 1
total_sun_time = 0
capacity = np.empty(len(power_consumption_minutes) + 1)
capacity[0] = variables.initial_capacity

# Battery capacity dynamic simulation
power = df["Power"].loc[all_start:(all_start + len(power_consumption_minutes) - 1)].to_numpy()
counter = 0
if args.demo:
    capacity = (power.cumsum() * 1/60 - power_consumption_minutes.cumsum() / 60).clip(None, variables.full_capacity)
else:
    for p in tqdm(power, desc="Capacity"):
        capacity[counter + 1] = min(capacity[counter] + p * 1/60 - power_consumption_minutes[counter] / 60, variables.full_capacity)
        counter += 1

if args.eclipse:
    for i in trange(all_start, all_end - 1, desc="Eclipse"):
        if df["Power"][i + 1] <= df["Power"][i] and df["Power"][i + 1] < 0.0001:
            counter_eclipse = counter_eclipse + 1
        else:
            if total_eclipse_time != (total_eclipse_time + counter_eclipse):
                total_eclipse_time = total_eclipse_time + counter_eclipse
                number_eclipses = number_eclipses + 1
            if eclipse_max < counter_eclipse:
                eclipse_max = counter_eclipse
            counter_eclipse = 0
        if df["Power"][i] > 0.0001 and df["Power"][i + 1] > 0.0001:
            counter_sun = counter_sun + 1
        else:
            if total_sun_time != (total_sun_time + counter_sun):
                total_sun_time = total_sun_time + counter_sun
                number_suns = number_suns + 1
            if sun_max < counter_sun:
                sun_max = counter_sun
                eclipse_min_search = i
            if sun_min > counter_sun > 0 and all_start + 100 < i < all_end - 100:  # Prevent data contamination at start/end
                sun_min = counter_sun
            counter_sun = 0

    average_eclipse = total_eclipse_time / number_eclipses
    average_sun = total_sun_time / number_suns
    print("Maximum eclipse duration for current orbit is: %.3f minutes" % eclipse_max)
    print("Average eclipse time is: %.3f minutes" % average_eclipse)
    print("Maximum sunlight duration for current orbit is: %.3f minutes" % sun_max)
    print("Minimum sunlight duration for current orbit is: %.3f minutes" % sun_min)
    print("Average sunlight duration is: %.3f minutes" % average_sun)

    logging.debug("Located longest eclipse at {}".format(eclipse_min_search))

    # Calculate minimum eclipse/total ratio

    eclipse_min = 1000
    counter_eclipse = 0

    for i in range(max(eclipse_min_search - 500, all_start), min(eclipse_min_search + 500, all_end - 1)):
        if df["Power"][i + 1] <= df["Power"][i] and df["Power"][i + 1] < 0.0001:
            counter_eclipse = counter_eclipse + 1
        else:
            if eclipse_min > counter_eclipse > 0:
                eclipse_min = counter_eclipse

    min_eclipse_percentage = eclipse_min / (sun_max + eclipse_min)
    print("Minimum ratio of eclipse/(eclipse+sunlight) is: %.3f" % (min_eclipse_percentage * 100))

    datapack["eclipse"] = {
        "eclipse_max": eclipse_max,
        "eclipse_average": average_eclipse,
        "eclipse_min": eclipse_min,
        "worst_eclipse_ratio": min_eclipse_percentage,
        "sun_max": sun_max,
        "sun_average": average_sun,
        "sun_min": sun_min,
    }

#plt.figure(3)
fig, ax = plt.subplots(figsize=[15, 15 / 20 * 9])

plt.xlabel('Time [days]', fontsize=22.5, y=0.01)
plt.ylabel('Battery charge [Wh]', fontsize=22.5, x=-0.01)
#plt.plot(df["Datetime"].head(len(capacity)).to_numpy(), capacity)
plt.plot(capacity)
plt.axhline(y=0.2 * variables.full_capacity, color='red', linestyle='--')

ax.tick_params(labelsize=14)
ax.xaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: str(int(x / 60 / 24))))

if capacity[-1000] > 0:
    axins = ax.inset_axes([0.5, 0.5, 0.47, 0.47])
    axins.set_position([0.5, 0.2, 0.45, 0.45])
    axins.plot(capacity)
    axins.set_axes_locator(_make_inset_locator([.5,.2,.25,.25], fig.transFigure, ax))

    axins.set_xlim(100000, 100350)
    axins.set_ylim(34.5, 37)
    axins.set_xticklabels('')
    axins.set_yticklabels('')
    ax.indicate_inset_zoom(axins, ec='red')


with open(args.output + '.json', 'w', encoding="utf-8") as outfile:
    opts = jsbeautifier.default_options()
    opts.indent_size = 2
    outfile.write(jsbeautifier.beautify(json.dumps(datapack, ensure_ascii=False)))

plt.grid()
plt.tight_layout()
plotter.plot(plt, "discharge", "Charge level")